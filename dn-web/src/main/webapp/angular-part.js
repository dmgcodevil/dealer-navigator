var preSelected = {};
angular.module('app', []).controller('dealersList', function ($scope, $timeout, $element, $http) {
    $http.get('stub-data/sales_persons.json').success(function (data) {
        $scope.sales = data;
        if (!preSelected.name) {
            angular.extend(preSelected,$scope.sales[0])
        }
        $timeout(function () {
            $($element).find('.button-dyn').button().click(function(){
                $.mobile.changePage('#DatePickersPage');
            })
        });
    });

    $scope.selectSales = function (sale) {
        if ($scope.selectedSale) {
            $scope.selectedSale.selected = false;
        }
        $scope.selectedSale = sale;
        $scope.selectedSale.selected = true;
        angular.extend(preSelected, $scope.selectedSale)
    };

    $scope.preSelected = preSelected;

    $scope.selectRandom = function(){
        var randomIndex = Math.round(($scope.sales.length - 1) * Math.random());
        $scope.selectSales($scope.sales[ randomIndex]);
    };
});
