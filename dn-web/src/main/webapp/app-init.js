$(document).ready(function (e) {
    $('[data-role=rangeslider]').on('swiperight', function () {
        return false;
    }).on('swipeleft', function () {
        return false
    }).change(function () {
        console.log(arguments)
        var value1 = $(this).find('input').first().val();
        var value2 = $(this).find('input').last().val();
        $('.timeBegin').text(value1 < 13 ? value1 + ' AM' : value1 - 12 + ' PM')
        $('.timeEnd').text(value2 < 13 ? value2 + ' AM' : value2 - 12 + ' PM')
    }).find('input').hide();

    $('select').change(function () {
        app.changeSelect($(this).attr('name'), $(this).val(), $(this).prev('[data-rel=dialog]').find('span').text());
    });
    $('.to-sort').sortable({
        'containment': 'parent',
        update: function () {
            var array = [];
            $(this).find('.val').each(function () {
                array.push($(this).text());
            });
            app.changeSort(array)
        }
    });

    function nextPage(element, isCustom, sendEmail) {
        var arg1 = element.attr('data-link-forward');
        console.log('arg' + arg1)
        if (isCustom) {
            return app[sendEmail ? 'sendEmail' : 'sendRest']().done(function (data) {
                if (!sendEmail) {
                    $('.dealerName').text(data.dealerName);
                    $('.dealerAddress').text(data.address);
                    $('#modelResponse').text(JSON.stringify(data));
                }
                if (arg1) {
                    $.mobile.changePage('#' + arg1);
                }
                app.logModel();
            }).fail(function (e) {
                console.error('error happened')
                console.log(e);
                $('.dealerName').text("Acura 101 West");
                $('.dealerAddress').text("California - 24650 Calabasas Rd");
                if (arg1) {
                    $.mobile.changePage('#' + arg1);
                }
            })
        } else {
            if (arg1) {
                $.mobile.changePage('#' + arg1);
            }
            app.logModel();
        }
    }

    $('[data-link-forward]').on('swipeleft', function () {
        var isCustom = $(this).attr('custom-page');
        nextPage($(this), isCustom);

    }).on('swiperight', function () {
        var arg = $(this).attr('data-link-backward');
        if (arg) {
            $.mobile.changePage('#' + arg);
        }

    });

    $('[data-link-forward]').each(function () {
        var page = $(this);
        var isCustom = page.attr('custom-page');
        page.find('.forward').click(function () {
            if (isCustom) {
                var button = $(this);
                button.css('opacity', '0.5');
                nextPage(page, isCustom).done(function () {
                    button.css('opacity', '1')
                }).fail(function(){
                    button.css('opacity', '1')
                });;
            } else {
                nextPage(page, isCustom)
            }

        });
        page.find('.book-time').click(function () {
            var button = $(this);
            button.css('opacity', '0.5');
            nextPage(page, isCustom, true).done(function () {
                button.css('opacity', '1')
            }).fail(function(){
                button.css('opacity', '1')
            });
        });
        var arg = page.attr('data-link-backward');
        page.find('.backward').click(function () {
            if (arg) {
                $.mobile.changePage('#' + arg);
            }
        });
    })
});
