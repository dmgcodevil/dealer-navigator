var app;
var model;
(function ($) {
    model = {
        make: 'Acura',
        model: '',
        year: 2014,
        order: [],
        day: 10,
        month: "09"
    };

    app = {
        update: function () {
            $('#model').text(JSON.stringify(model));
            $('.selectedValues').text(model.make + ' ' + model.modelText + ' ' + model.year);

            if (model.modelText) {
                $('.next-button').eq(0).show();
            }
            $('.chosenDate').text(model.month + '/' + model.day);
        },
        changeSelect: function (name, value, text) {
            model[name] = value;
            if (name == 'model') {
                console.log(text)
                model['modelText'] = text;
            }
            this.update();
        },
        changeSort: function (array) {
            model.order = array.map(function (item) {
                return item.split(' ').join('_').toUpperCase();
            });
            this.update();
        },
        logModel: function () {
            console.log(model);
        },
        sendRest: function () {
            return $.ajax({
                url: '/dn-rest/api/v1/dealer',
                type: "POST",
                data: JSON.stringify({
                    "styleId": model.model,
                    "make": "Acura",
                    "latitude": 34.149825,
                    "longitude": -118.662118,
                    "radius": 50,
                    "criteria": model.order
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function () {
                    /*{
                     "dealerId":1014,
                     "franchiseId":26925,
                     "dealerName":"Acura 101 West",
                     "make":"Acura",
                     "latitude":34.149825,"longitude":-118.662118,
                     "address":"91302, CA, Los Angeles, Calabasas, 24650 Calabasas Rd"
                     }*/
                }
            })
        },
        sendEmail: function () {
            return $.ajax({
                url: '/dn-rest/api/v1/notify/send',
                type: "POST",
                data: JSON.stringify({
                    "vehicle": model.modelText,
                    "dealerName": $('.dealerName').first().text(),
                    "dealerAddress": $('.dealerAddress').first().text(),
                    "salesName": preSelected.name,

                    // model.day model.month
                    date: $('.chosenDate').text() + ' ' + $('.timeBegin').text() + ' - ' + $('.timeEnd').text()
//                    "preferableStartDate": "14 10 27 00 91 869",
//                    "preferableEndDate": "14 10 27 00 91 869"
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            })
        }
    }

    $(function () {
        var array = [];
        $('.val').each(function () {
            array.push($(this).text());
        });
        app.changeSort(array);
    })

})(jQuery);
