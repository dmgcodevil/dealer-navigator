package com.hackathon.edmunds.dealernavigator;

import android.app.Activity;
import android.content.Context;
import android.net.DhcpInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.hackathon.edmunds.dealernavigator.util.AssetsPropertyReader;

import java.util.Properties;


public class MainActivity extends Activity {

    private AssetsPropertyReader assetsPropertyReader;
    private Context context;
    private Properties p;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;
        assetsPropertyReader = new AssetsPropertyReader(context);
        p = assetsPropertyReader.getProperties("dn.properties");
        WebView dnWebView = (WebView) findViewById(R.id.dnWebView);
        dnWebView.setWebChromeClient(new WebChromeClient());
        dnWebView.setWebViewClient(new WebViewClient());
        dnWebView.clearCache(true);
        dnWebView.clearHistory();
        dnWebView.getSettings().setJavaScriptEnabled(true);
        dnWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);


        WifiManager wifii = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        DhcpInfo d = wifii.getDhcpInfo();


        String ip = intToIP(d.serverAddress);

        StringBuilder stringBuilder = new StringBuilder("http://");
        stringBuilder.append(ip).append(":8080/dn-web/index.html");

        //http://192.168.137.1:8190/dn-web
        String siteUrl = stringBuilder.toString();


        dnWebView.loadUrl(siteUrl);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    static final int IP_ADDRESS_LENGTH = 32;

    public static String getSystemWifiIpAddress(Context context) {
        WifiManager wManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wInfo = wManager.getConnectionInfo();
        String wifiName = wInfo.getSSID();
        Log.e("IP in Mask Integer", wInfo.getIpAddress() + "");
        return intToIP(wInfo.getIpAddress());

    }

    public static String intToIP(int i) {
        return ((i & 0xFF) + "." + ((i >> 8) & 0xFF) +
                "." + ((i >> 16) & 0xFF) + "." + ((i >> 24) & 0xFF));
    }
}
