package com.edmunds.hackathon.dn.mapping;


import java.io.InputStream;
import java.util.Collection;

/**
 * Mapper to convert object into json and vise versa.
 * Created by dmgcodevil on 24.8.14.
 */
public interface JsonMapper {

    /**
     * Reads object from json string.
     *
     * @param json the string that represents json
     * @param type the type of object to convert
     * @param <T>  class type
     * @return object
     */
    <T> T readObject(String json, Class<? extends T> type);

    /**
     * Reads object from stream.
     *
     * @param is   the input stream
     * @param type the type of object to convert
     * @param <T>  class type
     * @return object
     */
    <T> T readObject(InputStream is, Class<? extends T> type);

    <T, C extends Collection<T>> C readObject(InputStream is, Class<? extends Collection> collectionType, Class<T> elementClass);
}