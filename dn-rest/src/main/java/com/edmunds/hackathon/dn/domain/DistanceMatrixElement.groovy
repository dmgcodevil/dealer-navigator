package com.edmunds.hackathon.dn.domain

/**
 * Created by Raman_Pliashkou on 9/9/2014.
 */
class DistanceMatrixElement {

    Distance distance;
    Duration duration;
    String status;


    @Override
    public String toString() {
        return """\
DistanceMatrixElement{
    distance=$distance,
    duration=$duration,
    status='$status'
}"""
    }
}
