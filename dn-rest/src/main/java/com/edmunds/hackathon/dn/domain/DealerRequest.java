package com.edmunds.hackathon.dn.domain;

import java.util.List;

public class DealerRequest {

	private Long styleId;
	private String make;
	private Double latitude;
	private Double longitude;
	private Integer radius;
	private List<DealerCriteria> criteria;

	public Long getStyleId() {
		return styleId;
	}
	public void setStyleId(Long styleId) {
		this.styleId = styleId;
	}
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Integer getRadius() {
		return radius;
	}
	public void setRadius(Integer radius) {
		this.radius = radius;
	}
	public List<DealerCriteria> getCriteria() {
		return criteria;
	}
	public void setCriteria(List<DealerCriteria> criteria) {
		this.criteria = criteria;
	}
}
