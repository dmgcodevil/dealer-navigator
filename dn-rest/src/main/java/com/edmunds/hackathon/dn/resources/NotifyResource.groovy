package com.edmunds.hackathon.dn.resources

import com.edmunds.hackathon.dn.domain.Notification
import org.springframework.stereotype.Component

import javax.mail.*
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage
import javax.ws.rs.Consumes
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

/**
 * Created by Raman_Pliashkou on 9/9/2014.
 */
@Path("notify")
@Component("notifyResource")
public class NotifyResource {

    /**
     * Sends notification to the dealer.
     *{* "vehicle": "test vehicle",
     * "dealerName": "test dealr",
     * "dealerAddress": "test address",
     * "salesName": "test sales name",
     * "date": "1410270091869",
     *}*
     * @param notification the notification
     * @return result: successful or not
     */
    @POST
    @Path("/send")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response sendNotification(Notification notification) {
        // todo add validation and throw badRequestException if something goes wrong
        boolean result = doSend(notification);
        return result ? Response.ok().build() : Response.serverError().build();
    }

    private boolean doSend(Notification notification) {
        final String username = "edmunds.hackathon@gmail.com";
        final String password = "1235050793";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, [getPasswordAuthentication: {
            return new PasswordAuthentication(username, password)
        }] as Authenticator);

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("customer@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse("edmunds.hackathon@gmail.com"));

            def dealerName = notification.dealerName;
            def vehicle = notification.vehicle;
            def date = notification.date;
            def salesName = notification.salesName;
            message.setSubject("Dealer Navigator meeting request");

            String body = "Dear $dealerName,\n" +
                    "\n" +
                    "User $username is interested in car '$vehicle'. He wants to book meeting with $salesName. Preferrable time for him is next: $date.\n" +
                    "\n" +
                    "Sincerely yours,\n" +
                    "Navigation Dealer application";


            message.setText(body);
            Transport.send(message);
            return true;
        } catch (MessagingException e) {
            return false;
        }
    }

}
