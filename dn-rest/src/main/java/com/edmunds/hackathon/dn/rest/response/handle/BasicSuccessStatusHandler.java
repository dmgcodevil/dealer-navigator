package com.edmunds.hackathon.dn.rest.response.handle;


import com.edmunds.hackathon.dn.exception.HttpClientException;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;

/**
 * Created by dmgcodevil on 2.9.14.
 */
public class BasicSuccessStatusHandler implements HttpResponseHandler {

    @Override
    public void handle(HttpResponse response) throws HttpClientException {
        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            throw new HttpClientException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
        }
    }
}