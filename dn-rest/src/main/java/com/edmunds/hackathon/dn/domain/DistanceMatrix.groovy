package com.edmunds.hackathon.dn.domain

import com.fasterxml.jackson.annotation.JsonProperty
import com.google.common.collect.Lists;

/**
 * Created by Raman_Pliashkou on 9/9/2014.
 */
import com.google.common.collect.Sets

/**
 *{* "destination_addresses" : [ "2436 Colorado Avenue, Santa Monica, CA 90404, USA" ],
 * "origin_addresses" : [ "24486-24512 Calabasas Road, Calabasas, CA 91302, USA" ],
 * "rows" : [
 *{* "elements" : [
 *{* "distance" : {* "text" : "38.6 km",
 * "value" : 38607
 *},
 * "duration" : {* "text" : "30 mins",
 * "value" : 1787
 *},
 * "status" : "OK"
 *}* ]
 *}* ],
 * "status" : "OK"
 *}*/
class DistanceMatrix {

    //destination_addresses
    @JsonProperty("destination_addresses")
    Set<String> destinationAddresses = Sets.newHashSet();

    @JsonProperty("origin_addresses")
    Set<String> originAddresses = Sets.newHashSet();


    List<Row> rows = Lists.newArrayList();

    String status;


    @Override
    public String toString() {
        return """\
                DistanceMatrix{
                    destinationAddresses=$destinationAddresses,
                    originAddresses=$originAddresses,
                    rows=$rows,
                    status='$status'
                }"""
    }
}
