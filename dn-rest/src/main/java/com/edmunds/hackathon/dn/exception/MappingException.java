package com.edmunds.hackathon.dn.exception;

/**
 * Created by Raman_Pliashkou on 9/9/2014.
 */
public class MappingException extends RuntimeException {
    public MappingException() {
    }

    public MappingException(String message) {
        super(message);
    }

    public MappingException(String message, Throwable cause) {
        super(message, cause);
    }

    public MappingException(Throwable cause) {
        super(cause);
    }

}
