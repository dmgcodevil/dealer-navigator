package com.edmunds.hackathon.dn.rest.request;

import com.google.common.collect.ImmutableList;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raman_Pliashkou on 9/9/2014.
 */
public class RequestParameters {

    private final List<NameValuePair> pathParams = new ArrayList<>();
    private final List<NameValuePair> queryParams = new ArrayList<>();
    private final static RequestParameters EMPTY = new EmptyRequestParameters();

    public static RequestParameters empty() {
        return EMPTY;
    }

    public static RequestParameters create() {
        return new RequestParameters();
    }

    public RequestParameters addPathParam(String name, Object val) {
        pathParams.add(new NameValuePair(name, val.toString()));
        return this;
    }

    public RequestParameters addQueryParam(String name, Object val) {
        queryParams.add(new NameValuePair(name, val.toString()));
        return this;
    }

    public List<NameValuePair> getPathParams() {
        return ImmutableList.copyOf(pathParams);
    }

    public List<NameValuePair> getQueryParams() {
        return ImmutableList.copyOf(queryParams);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("pathParams=").append('[').append(toString(pathParams)).append(']');
        sb.append(", queryParams=").append('[').append(toString(queryParams)).append(']');
        return sb.toString();
    }

    public static class NameValuePair {
        private final String name;
        private final String value;

        private NameValuePair(String name, String value) {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return new StringBuilder()
                    .append("name='").append(name).append('\'')
                    .append(", value='").append(value).append('\'').toString();
        }
    }

    private String toString(List<NameValuePair> pairs) {
        if (CollectionUtils.isEmpty(pairs)) {
            return StringUtils.EMPTY;
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (NameValuePair pair : pairs) {
            stringBuilder.append(pair.toString()).append(",");
        }
        String result = stringBuilder.toString();
        return result.substring(0, result.length() - 1);
    }

    private static class EmptyRequestParameters extends RequestParameters {

        private static final String MESSAGE =
                "this is EmptyRequestParameters implementation and it's prohibited be modified. Create new instance of RequestParameters";

        @Override
        public RequestParameters addPathParam(String name, Object val) {
            throw new UnsupportedOperationException(MESSAGE);
        }

        @Override
        public RequestParameters addQueryParam(String name, Object val) {
            throw new UnsupportedOperationException(MESSAGE);
        }
    }
}
