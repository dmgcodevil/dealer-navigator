package com.edmunds.hackathon.dn.domain

/**
 * Created by Raman_Pliashkou on 9/9/2014.
 */
class Duration {
    String text;
    Integer value;


    @Override
    public String toString() {
        return """\
Duration{
    text='$text',
    value=$value
}"""
    }
}
