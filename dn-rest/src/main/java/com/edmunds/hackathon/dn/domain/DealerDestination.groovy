package com.edmunds.hackathon.dn.domain

/**
 * Created by Raman_Pliashkou on 9/9/2014.
 */
class DealerDestination {

    String id;
    String destination;

    DealerDestination(String id, String destination) {
        this.id = id
        this.destination = destination
    }
}
