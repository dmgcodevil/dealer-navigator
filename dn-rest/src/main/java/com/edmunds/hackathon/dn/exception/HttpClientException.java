package com.edmunds.hackathon.dn.exception;

/**
 * Created by Raman_Pliashkou on 9/9/2014.
 */
public class HttpClientException extends RuntimeException {
    public HttpClientException() {
    }

    public HttpClientException(String message) {
        super(message);
    }

    public HttpClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public HttpClientException(Throwable cause) {
        super(cause);
    }

}
