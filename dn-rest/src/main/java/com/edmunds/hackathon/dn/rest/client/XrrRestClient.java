package com.edmunds.hackathon.dn.rest.client;

import com.edmunds.hackathon.dn.rest.HttpTemplate;
import com.edmunds.hackathon.dn.rest.request.RequestParameters;
import com.edmunds.rest.dto.xrr.dealerreview.AverageRatingDto;
import com.edmunds.rest.dto.xrr.dealerreview.AverageRatingsDto;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.testng.Assert;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;

/**
 * Created by Raman_Pliashkou on 9/9/2014.
 */
@Component
public class XrrRestClient {

    private final HttpTemplate httpTemplate = new HttpTemplate();

    private static final String RATINGS_BY_ID = "http://www.edmunds.com/api/dealerreviews/v2/dealers/{dealerId}/ratings/average";

    private static final String RATINGS_BY_IDS = "http://www.edmunds.com/api/dealerreviews/v2/ratings/average";


    /**
     * Gets average rating by dealer id.
     *
     * @param dealerId the dealer id
     * @return {@link AverageRatingDto}
     */
    public AverageRatingDto getAverageRating(String dealerId) {
        RequestParameters parameters = RequestParameters.create().addPathParam("dealerId", dealerId);
        AverageRatingDto averageRating = httpTemplate.getForObject(RATINGS_BY_ID, parameters, AverageRatingDto.class);
        System.out.println(averageRating);
        return averageRating;
    }

    /**
     * Gets average rating by dealer id.
     *
     * @param dealerIds the set of dealer ids
     * @return {@link AverageRatingDto}
     */
    public Map<String, Float> getSalesAverageRatings(Set<String> dealerIds) {
        if (CollectionUtils.isEmpty(dealerIds)) {
            return Collections.emptyMap();
        }
        Map<String, Float> salesAverageRatings = Maps.newHashMap();
        RequestParameters parameters = RequestParameters.create()
                .addQueryParam("dealerId", StringUtils.join(dealerIds, ","))
                .addQueryParam("type", "sales");
        AverageRatingsDto averageRatings = httpTemplate.getForObject(RATINGS_BY_IDS, parameters, AverageRatingsDto.class);
        if (MapUtils.isEmpty(averageRatings.getAverageRatings())) {
            return Collections.emptyMap();
        }
        for (Map.Entry<String, AverageRatingDto> avg : averageRatings.getAverageRatings().entrySet()) {
            String dealerId = avg.getKey();
            Float sAvgRating = 0f;
            if (avg.getValue().getSalesReview() != null) {
                sAvgRating = avg.getValue().getSalesReview().getAverageRating();
            }

            salesAverageRatings.put(dealerId, sAvgRating);
        }

        return salesAverageRatings;
    }


    public static class Test {
        @org.testng.annotations.Test
        public void testByDealerId() {
            final XrrRestClient xrrRestClient = new XrrRestClient();
            AverageRatingDto averageRatingDto = xrrRestClient.getAverageRating("1014");
            Assert.assertNotNull(averageRatingDto);
        }

        @org.testng.annotations.Test
        public void testByDealerIds() {
            final XrrRestClient xrrRestClient = new XrrRestClient();
            Map<String, Float> salesAverageRatings = xrrRestClient.getSalesAverageRatings(Sets.newHashSet("331", "645", "640"));
            Assert.assertNotNull(salesAverageRatings);
            Assert.assertNotNull(salesAverageRatings.get("331"));
        }
    }
}
