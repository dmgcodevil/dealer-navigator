package com.edmunds.hackathon.dn.domain;

import org.apache.solr.client.solrj.beans.Field;

public class Inventory {

	private String dealerId;
	private String franchiseId;
	private String vin;
	private Double guaranteedPrice;

	public String getDealerId() {
		return dealerId;
	}
	
	@Field("dealerId")
	public void setDealerId(String dealerId) {
		this.dealerId = dealerId;
	}

	public String getFranchiseId() {
		return franchiseId;
	}

	@Field("franchiseId")
	public void setFranchiseId(String franchiseId) {
		this.franchiseId = franchiseId;
	}

	public String getVin() {
		return vin;
	}

	@Field("vin")
	public void setVin(String vin) {
		this.vin = vin;
	}

	public Double getGuaranteedPrice() {
		return guaranteedPrice;
	}
	
	@Field("prices_|NCI|#price_|guaranteed-price|")
	public void setGuaranteedPrice(Double guaranteedPrice) {
		this.guaranteedPrice = guaranteedPrice;
	}
}
