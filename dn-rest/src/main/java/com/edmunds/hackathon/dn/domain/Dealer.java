package com.edmunds.hackathon.dn.domain;

import org.apache.solr.client.solrj.beans.Field;

public class Dealer {

	private String dealerId;
	private String franchiseId;
	private String dealerName;
	private String make;
	private Double latitude;
	private Double longitude;
	private String stateCode;
	private String county;
	private String city;
	private String zip;
	private String street;

	public Dealer() {}

	public Dealer(String dealerId, String franchiseId, String dealerName, String make, Double latitude, Double longitude,
		String stateCode, String county, String city, String zip, String street) {
		this.dealerId = dealerId;
		this.franchiseId = franchiseId;
		this.dealerName = dealerName;
		this.make = make;
		this.latitude = latitude;
		this.longitude = longitude;
		this.stateCode = stateCode;
		this.county = county;
		this.city = city;
		this.zip = zip;
		this.street = street;
	}

	public String getDealerId() {
		return dealerId;
	}
	
	@Field("locationId")
	public void setDealerId(String dealerId) {
		this.dealerId = dealerId;
	}

	public String getFranchiseId() {
		return franchiseId;
	}
	
	@Field("uniqueKey")
	public void setFranchiseId(String franchiseId) {
		this.franchiseId = franchiseId;
	}

	public String getMake() {
		return make;
	}

	@Field("make")
	public void setMake(String make) {
		this.make = make;
	}

	public Double getLatitude() {
		return latitude;
	}

	@Field("location_0_coordinate")
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	@Field("location_1_coordinate")
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getDealerName() {
		return dealerName;
	}

	@Field("name")
	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}

	public String getStateCode() {
		return stateCode;
	}

	@Field("address.statecode")
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getCounty() {
		return county;
	}

	@Field("address.county")
	public void setCounty(String county) {
		this.county = county;
	}

	public String getCity() {
		return city;
	}

	@Field("address.city")
	public void setCity(String city) {
		this.city = city;
	}

	public String getZip() {
		return zip;
	}

	@Field("address.zipcode")
	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getStreet() {
		return street;
	}

	@Field("address.street")
	public void setStreet(String street) {
		this.street = street;
	}

	public String getAddress() {
		return (street + ", " + city + ", " + zip + ", " + stateCode);
	}
}
