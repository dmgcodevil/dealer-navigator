package com.edmunds.hackathon.dn.rest.client;

import com.edmunds.hackathon.dn.domain.DealerDestination;
import com.edmunds.hackathon.dn.domain.DistanceMatrix;
import com.edmunds.hackathon.dn.domain.Row;
import com.edmunds.hackathon.dn.exception.BadRequestException;
import com.edmunds.hackathon.dn.rest.HttpTemplate;
import com.edmunds.hackathon.dn.rest.request.RequestParameters;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.testng.Assert;

import java.util.Map;
import java.util.Set;

/**
 * Created by Raman_Pliashkou on 9/9/2014.
 */
@Component
public class GoogleMapClient {


    private static final String BASE_URL = "http://maps.googleapis.com/maps/api";

    private static final String DISTANCE_MATRIX = BASE_URL + "/distancematrix/{format}";

    private static final String FORMAT = "format";
    private static final String JSON = "json";

    private final HttpTemplate httpTemplate = new HttpTemplate();

    /**
     * The Google Distance Matrix API is a service that provides travel distance and time for a matrix of origins and destinations.
     * The information returned is based on the recommended route between start and end points, as calculated by the Google Maps API,
     * and consists of rows containing duration and distance values for each pair.
     *
     * @param origins      one or more addresses and/or textual latitude/longitude values, separated with the pipe (|) character, from which to calculate distance and time.
     * @param destinations one or more addresses and/or textual latitude/longitude values, separated with the pipe (|) character, to which to calculate distance and time.
     * @return {@link DistanceMatrix}
     */
    public DistanceMatrix getDistanceMatrix(String origins, String destinations) {
        if (StringUtils.isBlank(origins) || StringUtils.isBlank(destinations)) {
            throw new BadRequestException("Both of the `origins`, `destinations` parameters should be specified.");
        }
        return httpTemplate.getForObject(DISTANCE_MATRIX, defaultParams()
                .addQueryParam("origins", origins)
                .addQueryParam("destinations", destinations)
                .addQueryParam("mode", "driving")
                .addQueryParam("language", "en-ENG")
                .addQueryParam("sensor", "false")
                , DistanceMatrix.class);
    }

    public Map<String, Integer> getDistanceMatrix(String origins, Set<DealerDestination> dealerDestinations) {
        if (StringUtils.isBlank(origins) || CollectionUtils.isEmpty(dealerDestinations)) {
            throw new BadRequestException("Both of the `origins`, `destinations` parameters should be specified.");
        }
        Map<String, Integer> distances = Maps.newHashMap();
        for (DealerDestination dealerDestination : dealerDestinations) {
            DistanceMatrix dMatrix = getDistanceMatrix(origins, dealerDestination.getDestination());
            Integer distance = 0;
            if (CollectionUtils.isNotEmpty(dMatrix.getRows())) {
                Row row = dMatrix.getRows().iterator().next();
                if (CollectionUtils.isNotEmpty(row.getElements())) {
                    distance = row.getElements().iterator().next().getDistance().getValue();
                }
            }
            distances.put(dealerDestination.getId(), distance);
        }
        return distances;
    }

    private static RequestParameters defaultParams() {
        return RequestParameters.create().addPathParam(FORMAT, JSON);
    }


    public static class Test {
        @org.testng.annotations.Test
        public void test() {
            GoogleMapClient googleMapClient = new GoogleMapClient();
            DistanceMatrix distanceMatrix = googleMapClient.getDistanceMatrix("34.149825,-118.662118", "34.02909,-118.4716");
            Assert.assertNotNull(distanceMatrix);
            System.out.println(distanceMatrix);
        }

        @org.testng.annotations.Test
        public void testGetDistanceMatrix() {
            GoogleMapClient googleMapClient = new GoogleMapClient();
            Map<String, Integer> distances = googleMapClient.getDistanceMatrix("34.149825,-118.662118", Sets.<DealerDestination>newHashSet(new DealerDestination("1014", "34.02909,-118.4716")));
            Assert.assertNotNull(distances);
        }
    }

}
