package com.edmunds.hackathon.dn.resources;

import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("test")
@Component("testResource")
public class TestResource {


    @GET
    @Path("/check")
    @Produces(MediaType.APPLICATION_JSON)
    public Object getUsers() {
        return "hello hackathon";
    }

}
