package com.edmunds.hackathon.dn.domain

import com.google.common.collect.Lists

/**
 * Created by Raman_Pliashkou on 9/9/2014.
 */
class Row {

     List<DistanceMatrixElement> elements = Lists.newArrayList();


    @Override
    public String toString() {
        return """\
Row{
    elements=$elements
}"""
    }
}
