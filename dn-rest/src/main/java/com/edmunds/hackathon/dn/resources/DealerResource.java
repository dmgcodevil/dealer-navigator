package com.edmunds.hackathon.dn.resources;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.edmunds.hackathon.dn.domain.Dealer;
import com.edmunds.hackathon.dn.domain.DealerCriteria;
import com.edmunds.hackathon.dn.domain.DealerDestination;
import com.edmunds.hackathon.dn.domain.DealerRequest;
import com.edmunds.hackathon.dn.domain.Inventory;
import com.edmunds.hackathon.dn.rest.client.GoogleMapClient;
import com.edmunds.hackathon.dn.rest.client.XrrRestClient;
import com.edmunds.hackathon.dn.service.SearchService;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

@Component
@Path("dealer")
public class DealerResource {

	private double MILES_TO_KILOMETERS = 1.61;

	private Dealer DEFAULT_DEALER = new Dealer("1014", "26925", "Acura 101 West", "Acura", 34.149825, -118.662118,
		"CA", "Los Angeles", "Calabasas", "91302", "24650 Calabasas Rd");

    private final SearchService dealerSearchService;
    private final SearchService inventorySearchService;
    private final GoogleMapClient googleMapClient;
    private final XrrRestClient xrrRestClient;

    @Autowired
    public DealerResource(@Qualifier("dealerSearchService") SearchService dealerSearchService,
    	@Qualifier("inventorySearchService") SearchService inventorySearchService,
    	GoogleMapClient googleMapClient, XrrRestClient xrrRestClient) {
        this.dealerSearchService = dealerSearchService;
        this.inventorySearchService = inventorySearchService;
        this.googleMapClient = googleMapClient;
        this.xrrRestClient = xrrRestClient;   
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Dealer getBestFranchise(DealerRequest dealer) {
    	try {
            List<Dealer> nearestDealers = getNearestDealers(dealer);
            final Map<String, Double> pricePromises = getMinPricePromises(dealer.getStyleId(), nearestDealers);
            Collection<Dealer> filteredDealers = Collections2.filter(nearestDealers, new Predicate<Dealer>() {
			    @Override
			    public boolean apply(Dealer dealer) {
				    return pricePromises.containsKey(dealer.getFranchiseId());
			    }
            });
        
            final Map<DealerCriteria, LoadableDealerPredicate> predicates = predicateMap(pricePromises);
        
            for(DealerCriteria dc :  dealer.getCriteria()){
        	    LoadableDealerPredicate predicate = predicates.get(dc);
        	    predicate.load(filteredDealers, dealer.getLatitude() + "," + dealer.getLongitude());
        	    filteredDealers = Collections2.filter(filteredDealers, predicate);
        	    if (filteredDealers.size() == 1) {
        		    return filteredDealers.iterator().next();
        	    } else if (filteredDealers.size() == 0) {
        		    // Use default dealer to prevent errors on presentation.
        		    return DEFAULT_DEALER;
        	    }
            }
            return filteredDealers.iterator().next();
    	} catch (Throwable ex) {
    		return DEFAULT_DEALER;
    	}          
    }
    
    private Map<DealerCriteria, LoadableDealerPredicate> predicateMap(final Map<String, Double> pricePromises) {
        Map<DealerCriteria, LoadableDealerPredicate> predicates = new EnumMap<>(DealerCriteria.class);
        predicates.put(DealerCriteria.PRICE_PROMISE, new LoadableDealerPredicate() {
        	private Map<String, Double> prices;
 			public boolean apply(Dealer input) {
				return (prices.get(input.getFranchiseId()) <= Collections.min(prices.values()) * 1.01);
			}
			public void load(Collection<Dealer> dealers, String... options) {
				this.prices = pricePromises;
			}
        });
        predicates.put(DealerCriteria.DISTANCE, new LoadableDealerPredicate() {
        	private Map<String, Integer> distances;
			public boolean apply(Dealer input) {
				return (distances.get(input.getDealerId()) <= Collections.min(distances.values()) * 1.1);
			}
			public void load(Collection<Dealer> dealers, String... options) {
		        this.distances = getDistances(options[0], dealers);
			}
        });
        predicates.put(DealerCriteria.DEALER_RATING, new LoadableDealerPredicate() {
            private Map<String, Float> ratings;     	
			public boolean apply(Dealer input) {
				return (ratings.get(input.getDealerId()) >= Collections.max(ratings.values()) * 0.95);
			}
			public void load(Collection<Dealer> dealers, String... options) {
				this.ratings = getRatings(dealers);
			}
        });
        return predicates;
    }
    
    private List<Dealer> getNearestDealers(DealerRequest dealer) {
        String point = dealer.getLatitude() + "," + dealer.getLongitude();
        String fields = "locationId,uniqueKey,name,make,location_0_coordinate,location_1_coordinate,address.street,"
        	+ "address.city,address.statecode,address.county,address.zipcode";

        String query = "q=*:*&fq=make:" + dealer.getMake() +
        	"&fq=locationType:DEALERFRANCHISE&fq=dealerFranchiseType:NEW&fq=pp_status:active&fq={!geofilt cache=false cost=100}&sort=geodist() asc" +
        	"&pt=" + point + "&sfield=location&d=" + (dealer.getRadius() * MILES_TO_KILOMETERS) + "&fl=" + fields + "&rows=10000";
        return dealerSearchService.queryResults(query, Dealer.class);
    }

    private Map<String, Double> getMinPricePromises(Long styleId, List<Dealer> franchises) {
    	String fields = "dealerId,franchiseId,vin,prices_*";
    	Map<String, Double> prices = new HashMap<>();
    	for (Dealer franchise : franchises) {
    		String query = "q=franchiseId:" + franchise.getFranchiseId() + "&q=pp_status:active&q=mappings_|style-id|:" +
    	        styleId + "&fl=" + fields + "&rows=1000";
    		List<Inventory> inventories = inventorySearchService.queryResults(query, Inventory.class);
    		if (!inventories.isEmpty()) {
    			prices.put(franchise.getFranchiseId(), Collections.min(inventories, new Comparator<Inventory>() {
					@Override
					public int compare(Inventory o1, Inventory o2) {
						if (o1.getGuaranteedPrice() == null) {
							return 1;
						}
						if (o2.getGuaranteedPrice() == null) {
							return -1;
						}
						return new Double(o1.getGuaranteedPrice() - o2.getGuaranteedPrice()).intValue();
					}
				}).getGuaranteedPrice());
    		}
    	}
    	return prices;
    	
        /*
    	String fields = "dealerId,franchiseId,vin,prices_|NCI|#price_|guaranteed-price|";
        Map<String, Double> prices = new HashMap<>();
    	for (Dealer franchise : franchises) {
    		String query = "q=franchiseId:" + franchise.getFranchiseId() + "&q=pp_status:active&q=mappings_|style-id|:" +
    	        styleId + "&sort=prices_|NCI|#price_|guaranteed-price| asc&fl=" + fields + "&rows=1";
    		List<Inventory> inventories = inventorySearchService.queryResults(query, Inventory.class);
    		if (!inventories.isEmpty()) {
    			prices.put(franchise.getFranchiseId(), inventories.get(0).getGuaranteedPrice());
    		}
    	}
    	return prices;
    	*/
    }
    
    private Map<String, Integer> getDistances(String origin, Collection<Dealer> dealers) {
    	return googleMapClient.getDistanceMatrix(origin, Sets.newHashSet(Iterables.transform(dealers, new Function<Dealer, DealerDestination>() {
			@Override
			public DealerDestination apply(Dealer dealer) {
				return new DealerDestination(dealer.getDealerId(), dealer.getLatitude() + "," + dealer.getLongitude());
			}
    	})));
    }

    private Map<String, Float> getRatings(Collection<Dealer> dealers) {
    	return xrrRestClient.getSalesAverageRatings(Sets.newHashSet(Iterables.transform(dealers, new Function<Dealer, String>() {
			@Override
			public String apply(Dealer dealer) {
				return dealer.getDealerId();
			}
    	})));
    }

    private interface LoadableDealerPredicate extends Predicate<Dealer> {
    	void load(Collection<Dealer> dealers, String... options);
    }
}
