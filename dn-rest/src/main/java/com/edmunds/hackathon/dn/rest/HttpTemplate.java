package com.edmunds.hackathon.dn.rest;

/**
 * Created by Raman_Pliashkou on 9/9/2014.
 */


import com.edmunds.hackathon.dn.exception.HttpClientException;
import com.edmunds.hackathon.dn.mapping.JsonMapper;
import com.edmunds.hackathon.dn.mapping.jackson.JacksonJsonMapper;
import com.edmunds.hackathon.dn.rest.request.RequestParameters;
import com.edmunds.hackathon.dn.rest.response.handle.BasicSuccessStatusHandler;
import com.edmunds.hackathon.dn.rest.response.handle.HttpResponseHandler;
import com.google.common.annotations.Beta;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;


/**
 * Http client to perform http operations: GET, POST, etc.
 * Created by dmgcodevil on 24.8.14.
 */
@Beta
public class HttpTemplate {

    private HttpClient httpClient = HttpClientBuilder.create().build();

    private JsonMapper jsonMapper = new JacksonJsonMapper();

    private HttpResponseHandler basicSuccessStatusHandler = new BasicSuccessStatusHandler();


    public HttpTemplate(HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    public HttpTemplate() {
    }

    /**
     * Sends GET request to specified url.
     *
     * @param url the url to make GET request
     * @return response {@link org.apache.http.HttpResponse}
     */
    public HttpResponse get(String url) throws HttpClientException {
        return get(url, RequestParameters.empty());
    }

    /**
     * Executes a request and return response as stream.
     *
     * @param url request url
     * @return response as stream
     */
    public HttpResponse get(String url, RequestParameters requestParameters) throws HttpClientException {
        HttpGet httpGet;
        try {
            url = setPathParams(url, requestParameters);
            httpGet = new HttpGet(url);
            URIBuilder uriBuilder = addQueryParams(new URIBuilder(httpGet.getURI()), requestParameters);
            httpGet.setURI(uriBuilder.build());
            HttpResponse response = httpClient.execute(httpGet);
            return handleResponse(response);
        } catch (Throwable e) {
            throw new HttpClientException("failed build url: " + url + ", parameters: " + requestParameters, e);
        }
        //finally ->  httpGet.abort(); this operation closes socket as well, don't invoke it before read data from socket
    }

    /**
     * Sends GET request to specified url.
     *
     * @param url        the url to make GET request
     * @param parameters the request parameters
     * @return input stream that contains response data
     */
    public InputStream getForStream(String url, RequestParameters parameters) throws HttpClientException {
        HttpResponse response = get(url, parameters);
        try {
            return response.getEntity().getContent();
        } catch (IOException e) {
            throw new HttpClientException(e);
        }
    }


    /**
     * Sends GET request to specified url.
     *
     * @param url the url to make GET request
     * @return input stream that contains response data
     */
    public InputStream getForStream(String url) throws HttpClientException {
        return getForStream(url, RequestParameters.empty());
    }

    /**
     * Executes a request and return an instance of specified class.
     *
     * @param url               request url
     * @param requestParameters the request parameters
     * @param responseType      response type
     * @param <T>               response type, parametrized Class
     * @return new instance of specified class
     */
    public <T> T getForObject(String url, RequestParameters requestParameters, Class<T> responseType) throws HttpClientException {
        InputStream response = getForStream(url, requestParameters);
        return convert(response, responseType);
    }

    /**
     * Executes a request and return an instance of specified class.
     *
     * @param url               request url
     * @param requestParameters the request parameters
     * @param <T>               response type, parametrized Class
     * @return new instance of specified class
     */
    public <T, C extends Collection<T>> C getForCollection(String url, RequestParameters requestParameters,
                                                           Class<? extends Collection> collectionType, Class<T> elementClass) throws HttpClientException {
        InputStream response = getForStream(url, requestParameters);
        return jsonMapper.readObject(response, collectionType, elementClass);
    }


    /**
     * Executes a request and return an instance of specified class.
     *
     * @param url          request url
     * @param responseType response type
     * @param <T>          response type, parametrized Class
     * @return new instance of specified class
     * @throws HttpClientException
     */
    public <T> T getForObject(String url, Class<T> responseType) throws HttpClientException {
        return getForObject(url, RequestParameters.empty(), responseType);
    }

    private <T> T convert(InputStream inputStream, Class<T> responseType) {
        return jsonMapper.readObject(inputStream, responseType);
    }

    private HttpResponse handleResponse(HttpResponse response) {
        basicSuccessStatusHandler.handle(response);
        return response;
    }

    /**
     * Replaces a placeholders in the given urlTemplate.
     *
     * @param urlTemplate the url template
     * @param parameters  the request parameters
     * @return processed url
     */
    private String setPathParams(String urlTemplate, RequestParameters parameters) {
        for (RequestParameters.NameValuePair nameValuePair : parameters.getPathParams()) {
            String pathParam = "{" + nameValuePair.getName() + "}";
            urlTemplate = urlTemplate.replace(pathParam, nameValuePair.getValue());
        }
        return urlTemplate;
    }

    private URIBuilder addQueryParams(URIBuilder uriBuilder, RequestParameters parameters) {
        for (RequestParameters.NameValuePair nameValuePair : parameters.getQueryParams()) {
            uriBuilder.addParameter(nameValuePair.getName(), nameValuePair.getValue());
        }
        return uriBuilder;
    }
}