package com.edmunds.hackathon.dn.mapping.jackson;


import com.edmunds.hackathon.dn.exception.MappingException;
import com.edmunds.hackathon.dn.mapping.JsonMapper;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

/**
 * This implementation is based on Jackson lib.
 * Created by dmgcodevil on 24.8.14.
 */
public class JacksonJsonMapper implements JsonMapper {

    private ObjectMapper objectMapper = new ObjectMapper();

    {
        objectMapper.setVisibilityChecker(objectMapper.getSerializationConfig().getDefaultVisibilityChecker()
                .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withCreatorVisibility(JsonAutoDetect.Visibility.NONE));
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        objectMapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
    }


    @Override
    public <T> T readObject(String json, Class<? extends T> type) {
        try {
            return objectMapper.readValue(json, type);
        } catch (IOException e) {
            throw new MappingException("failed to create object from json", e);
        }
    }

    @Override
    public <T> T readObject(InputStream is, Class<? extends T> type) {
        try {
            return objectMapper.readValue(is, type);
        } catch (IOException e) {
            throw new MappingException("failed to create object from input stream", e);
        }
    }

    @Override
    public <T, C extends Collection<T>> C readObject(InputStream is, Class<? extends Collection> collectionType, Class<T> elementClass) {
        JavaType type = objectMapper.getTypeFactory().
                constructCollectionType(collectionType, elementClass);
        try {
            return objectMapper.readValue(is, type);
        } catch (IOException e) {
            throw new MappingException("failed to create object from input stream", e);
        }
    }

    static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
}