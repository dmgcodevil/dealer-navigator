package com.edmunds.hackathon.dn.domain

/**
 * Created by Raman_Pliashkou on 9/9/2014.
 */
class Notification {
    String vehicle;
    String dealerName;
    String dealerAddress;
    String salesName;
    String date;;


    @Override
    public String toString() {
        return """\
                    Notification{
                        vehicle='$vehicle',
                        dealerName='$dealerName',
                        dealerAddress='$dealerAddress',
                        salesName='$salesName',
                        date=$date,
                    }"""
    }
}
