package com.edmunds.hackathon.dn.exception;

/**
 * Created by Raman_Pliashkou on 9/9/2014.
 */
public class BadRequestException extends RuntimeException {
    public BadRequestException(Throwable cause) {
        super(cause);
    }

    public BadRequestException() {
    }

    public BadRequestException(String message) {
        super(message);
    }

    public BadRequestException(String message, Throwable cause) {
        super(message, cause);
    }

}
