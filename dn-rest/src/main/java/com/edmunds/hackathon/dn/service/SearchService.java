package com.edmunds.hackathon.dn.service;

import java.util.List;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.servlet.SolrRequestParsers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edmunds.search.query.QueryBase;
import com.edmunds.search.service.SolrUtils;
import com.google.common.base.Function;

import static com.google.common.collect.Collections2.transform;

/**
 * Service contains common functionality for Solr services.
 *
 * @author Dzianis_Krauchanka.
 *
 */
public class SearchService {

    private static final Logger LOG = Logger.getLogger(SearchService.class);

    private SolrUtils solrUtils;

    private String solrURL;

    private static final Pattern REPLACEMENT_PATTERN = Pattern.compile("[|#-]");

    private static final Function<String, String> DELIM_CHAR_FILTER = new Function<String, String>() {
        @Override
        public String apply(String input) {
            return REPLACEMENT_PATTERN.matcher(input).replaceAll("*");
        }
    };

    /**
     * Constructor with default dependencies.
     *
     * @param solrUtils utility class for Solr calls.
     */
    @Autowired
    public SearchService(SolrUtils solrUtils) {
        this.solrUtils = solrUtils;
    }

    public <T> List<T> query(QueryBase query, Class<T> resultClass, String... fieldsToInclude) {
        SolrQuery solrQuery = new SolrQuery(query.toQuery());
        if (query.getRows() != null) {
            solrQuery.setRows(query.getRows());
        }
        if (!query.getSortArguments().isEmpty()) {
            solrQuery.setSort(query.getSortingField(), query.getSortingOrder());
        }
        solrQuery.setFields(fieldsToInclude.length == 0 ? getFieldNames(resultClass) : fieldsToInclude);
        return query(solrQuery, resultClass);
    }

    public <T> List<T> queryResults(String query, Class<T> resultClass) {
        return query(SolrRequestParsers.parseQueryString(query), resultClass);
    }

    <T> List<T> query(SolrParams params, Class<T> resultClass) {
        try {
            QueryResponse rsp = getSolrServer().query(params);
            return rsp.getBeans(resultClass);
        } catch (SolrServerException exception) {
            LOG.warn("Error running query:" + params.toString(), exception);
            throw new RuntimeException("Error running query: " + params.toString(), exception);
        }
    }

    <T> String[] getFieldNames(Class<T> resultClass) {
        try {
            return transform(getSolrServer().getBinder().toSolrInputDocument(resultClass.newInstance()).getFieldNames(),
                DELIM_CHAR_FILTER).toArray(new String[0]);
        } catch (Exception e) {
            return new String[0];
        }
    }
    
    public long queryCount(QueryBase query) {
        SolrQuery solrQuery = new SolrQuery(query.toQuery());
        solrQuery.setRows(0);
        try {
            QueryResponse rsp = getSolrServer().query(solrQuery);
            return rsp.getResults().getNumFound();
        } catch (SolrServerException exception) {
            LOG.warn("Error running query:" + solrQuery.toString(), exception);
            throw new RuntimeException("Error running query: " + solrQuery.toString(), exception);
        }
    }
    
    SolrServer getSolrServer() {
        return solrUtils.getSolrServerInstance(solrURL);
    }

    public void setSolrURL(String solrURL) {
        this.solrURL = solrURL;
    }
}
