package com.edmunds.hackathon.dn.domain;

public enum DealerCriteria {

	PRICE_PROMISE,
	DISTANCE,
	DEALER_RATING;
	
}
